<?php

namespace Skafandri\PerformanceMeterBundle;

use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;

class CacheWarmer implements CacheWarmerInterface
{

    /**
     * @var array
     */
    private $proxies = [];

    /**
     * @param array $proxies
     */
    public function addProxy($name, $proxy)
    {
        $this->proxies[$name] = $proxy;
    }

    /**
     * @inheritdoc
     */
    public function isOptional()
    {
        // TODO: Implement isOptional() method.
    }

    /**
     * Warms up the cache.
     *
     * @param string $cacheDir The cache directory
     */
    public function warmUp($cacheDir)
    {
        $proxyDir = $cacheDir . '/performance';

        if (!is_dir($proxyDir)) {
            mkdir($proxyDir);
        }
        foreach ($this->proxies as $name => $content) {
            file_put_contents($cacheDir . '/performance/' . $name . '.php', $content);
        }
    }
}
