<?php

namespace Skafandri\PerformanceMeterBundle\DependencyInjection\Compiler;

use Skafandri\PerformanceMeterBundle\EventDispatcherAwareConnection;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverridesDoctrineConnectionClass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('doctrine')) {
            return;
        }

        $cacheWarmerDefinition = $container->getDefinition('performance_meter.cache_warmer');
        $filesToLoad = [];

        foreach ($container->getParameter('doctrine.connections') as $name => $id) {
            $definition = $container->getDefinition($id);

            $params = $definition->getArgument(0);

            if (empty($params['wrapperClass'])) {
                $params['wrapperClass'] = EventDispatcherAwareConnection::class;
            } else {
                $fileName = 'Proxy_' . $name;
                $filesToLoad[] = $fileName;
                $namespace = 'Skafandri\PerformanceBundle\Proxy';
                $className = 'EventDispatcherAwareConnection_' . $name;


                $proxy = $this->generateProxy($namespace, $className, $params['wrapperClass']);
                $cacheWarmerDefinition->addMethodCall('addProxy', [$fileName, $proxy]);
                $params['wrapperClass'] = $namespace . '\\' . $className;
            }
            $definition->replaceArgument(0, $params);
        }

        $container->setParameter('performance_bundle.files_to_load', $filesToLoad);
    }


    private function generateProxy($namespace, $className, $class)
    {
        return <<<EOF
<?php

namespace $namespace;

use Doctrine\DBAL\Connection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class $className extends $class
{

    public function setEventDispatcher(EventDispatcherInterface \$dispatcher)
    {

    }
}
EOF;

    }
}
