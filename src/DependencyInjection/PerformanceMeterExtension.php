<?php

namespace Skafandri\PerformanceMeterBundle\DependencyInjection;

use Skafandri\PerformanceMeterBundle\RequestLogger;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class PerformanceMeterExtension extends Extension
{

    const LOGGER_TYPE_REQUEST = 'request';

    public function load(array $configs, ContainerBuilder $container)
    {
        $locator = new FileLocator(array(__DIR__ . '/../../Resources/config'));
        $loader = new XmlFileLoader($container, $locator);

        $config = $this->processConfiguration(new Configuration(), $configs);

        if (!$this->isConfigEnabled($container, $config)) {
            return;
        }

        $loader->load('services.xml');
        $this->loadRequestLoggers($config, $container);
    }

    private function loadRequestLoggers($config, ContainerBuilder $container)
    {
        $loggerReference = new Reference('logger', ContainerInterface::NULL_ON_INVALID_REFERENCE);
        //?logger in configuration

        $kernelSubscriberDefintion = $container->getDefinition('performance_meter.kernel_events_subscriber');
        foreach ($config['loggers'] as $loggerName => $logger) {
            if ($logger['type'] !== self::LOGGER_TYPE_REQUEST) {
                continue;
            }
            $loggerServiceId = 'performance_meter.' . $loggerName . '.request_logger';
            $definition = new Definition(RequestLogger::class, [$loggerReference]);
            $container->setDefinition($loggerServiceId, $definition);

            $kernelSubscriberDefintion->addMethodCall('addRequestLogger', [new Reference($loggerServiceId)]);

        }
    }
}
