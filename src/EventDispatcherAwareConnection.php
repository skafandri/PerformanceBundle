<?php

namespace Skafandri\PerformanceMeterBundle;

use Doctrine\DBAL\Connection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventDispatcherAwareConnection extends Connection
{

    public function setEventDispatcher(EventDispatcherInterface $dispatcher)
    {

    }
}
