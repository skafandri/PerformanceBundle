<?php

namespace Skafandri\PerformanceMeterBundle;

use Skafandri\PerformanceMeterBundle\DependencyInjection\Compiler\OverridesDoctrineConnectionClass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PerformanceMeterBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new OverridesDoctrineConnectionClass());
    }

    public function boot()
    {
        if ($this->container->hasParameter('performance_meter.files_to_load')) {
            foreach ($this->container->getParameter('performance_meter.files_to_load') as $path) {
                include $path;
            }
        }
    }
}
