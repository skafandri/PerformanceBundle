<?php

namespace Skafandri\PerformanceMeterBundle\Tests\DependencyInjection;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\DoctrineExtension;
use PHPUnit\Framework\TestCase;
use Skafandri\PerformanceMeterBundle\DependencyInjection\PerformanceMeterExtension;
use Skafandri\PerformanceMeterBundle\EventDispatcherAwareConnection;
use Skafandri\PerformanceMeterBundle\KernelEventsSubscriber;
use Skafandri\PerformanceMeterBundle\PerformanceMeterBundle;
use Skafandri\PerformanceMeterBundle\RequestLogger;
use Skafandri\PerformanceMeterBundle\Tests\DependencyInjection\Stubs\TestConnectionWrapper;
use Skafandri\PerformanceMeterBundle\Tests\DependencyInjection\Stubs\TestKernel;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\Reference;

class PerformanceMeterExtensionTest extends TestCase
{
    public function test_registers_request_logger()
    {
        $container = $this->createContainer();
        $container->compile();


        foreach (['logger1', 'logger2'] as $logger) {
            $requestLoggerDefinition = $container->getDefinition('performance_meter.' . $logger . '.request_logger');

            $this->assertEquals(RequestLogger::class, $requestLoggerDefinition->getClass());
        }


    }

    public function test_registers_kernel_event_subscriber()
    {
        $container = $this->createContainer();
        $container->compile();

        $eventSubscriberDefinition = $container->getDefinition('performance_meter.kernel_events_subscriber');

        $this->assertEquals(KernelEventsSubscriber::class, $eventSubscriberDefinition->getClass());

        $this->assertEquals(
            array('kernel.event_subscriber' => array(array())),
            $eventSubscriberDefinition->getTags()
        );

        $this->assertEquals(
            [
                [
                    'addRequestLogger', [new Reference('performance_meter.logger1.request_logger')]
                ],
                [
                    'addRequestLogger', [new Reference('performance_meter.logger2.request_logger')]
                ],
            ],
            $eventSubscriberDefinition->getMethodCalls()
        );
    }

    public function test_registers_nothing_when_disabled()
    {
        $container = $this->createContainer(['disabled.yml']);

        $container->compile();

        $this->assertFalse($container->has('performance_meter.request_logger'));
        $this->assertFalse($container->has('performance_meter.kernel_events_subscriber'));
    }

    public function test_overrides_doctrine_connections()
    {
        $container = $this->createContainer(['doctrine.yml'], [new DoctrineExtension()]);

        $container->compile();

        $connection1 = $container->get('doctrine.dbal.conn1_connection');
        $this->assertInstanceOf(EventDispatcherAwareConnection::class, $connection1);
        $connection2 = $container->get('doctrine.dbal.conn2_connection');
        $this->assertInstanceOf(EventDispatcherAwareConnection::class, $connection2);
    }


    public function test_preserves_connection_wrapper_class()
    {
        $container = $this->createContainer(['doctrine_wrapper_class.yml'], [new DoctrineExtension()]);

        $container->compile();

        $connection1 = $container->get('doctrine.dbal.conn1_connection');

        $this->assertInstanceOf(TestConnectionWrapper::class, $connection1);

    }

    public function test_proxies_user_wrapper_class()
    {
        $kernel = $this->createKernel();
        $connection = $kernel->getContainer()->get('doctrine.dbal.conn1_connection');

        $this->assertTrue(is_callable([$connection, 'setEventDispatcher']));
    }


    private function createKernel()
    {
        $kernel = new TestKernel('dev', true);
        $kernel->boot();
        return $kernel;
    }

    public function test_registers_cache_warmer()
    {
        $kernel = $this->createKernel();
        $kernel->getContainer()->get('cache_warmer');
    }

    private function createContainer(array $configs = [], array $extensions = [])
    {
        $container = new ContainerBuilder(new ParameterBag(['kernel.debug' => false]));

        $container->registerExtension(new PerformanceMeterExtension());

        foreach ($extensions as $extension) {
            $container->registerExtension($extension);
        }

        $locator = new FileLocator(__DIR__ . '/Fixtures');
        $loader = new YamlFileLoader($container, $locator);
        $loader->load('config.yml');

        foreach ($configs as $config) {
            $loader->load($config);
        }

        $performanceBundle = new PerformanceMeterBundle();
        $performanceBundle->build($container);

        return $container;
    }
}
