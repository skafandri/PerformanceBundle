<?php

namespace Skafandri\PerformanceMeterBundle\Tests\DependencyInjection\Stubs;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Skafandri\PerformanceMeterBundle\PerformanceMeterBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;

class TestKernel extends Kernel
{

    /**
     * Returns an array of bundles to register.
     *
     * @return BundleInterface[] An array of bundle instances
     */
    public function registerBundles()
    {
        return [
            new FrameworkBundle(),
            new DoctrineBundle(),
            new PerformanceMeterBundle()
        ];
    }

    /**
     * Loads the container configuration.
     *
     * @param LoaderInterface $loader A LoaderInterface instance
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir() . '/../Fixtures/doctrine_wrapper_class.yml');
    }
}